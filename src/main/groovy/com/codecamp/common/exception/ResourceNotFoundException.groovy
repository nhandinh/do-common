package com.codecamp.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Create by dhnhan on 28/12/2019
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
class ResourceNotFoundException extends RuntimeException {
  private String resourceName
  private String fieldName
  private Object fieldValue

  ResourceNotFoundException(String var1) {
    super(var1)
  }

  ResourceNotFoundException(String resourceName, String fieldName, Object fieldValue) {
    super("${resourceName} not found with ${fieldName} : ${fieldValue}")
    this.resourceName = resourceName
    this.fieldName = fieldName
    this.fieldValue = fieldValue
  }
}
