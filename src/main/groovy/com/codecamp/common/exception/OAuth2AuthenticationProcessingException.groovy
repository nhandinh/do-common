package com.codecamp.common.exception

import org.springframework.security.core.AuthenticationException

/**
 * Create by dhnhan on 28/12/2019
 */

class OAuth2AuthenticationProcessingException extends AuthenticationException {

  OAuth2AuthenticationProcessingException(String msg, Throwable t) {
    super(msg, t)
  }

  OAuth2AuthenticationProcessingException(String msg) {
    super(msg)
  }
}
