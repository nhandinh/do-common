package com.codecamp.common.exception

class FileException extends RuntimeException {

  FileException(String var1) {
    super(var1)
  }

  FileException(String var1, Throwable var2) {
    super(var1, var2)
  }
}
