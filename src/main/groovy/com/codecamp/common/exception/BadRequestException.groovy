package com.codecamp.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Create by dhnhan on 28/12/2019
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException extends RuntimeException {

  BadRequestException(String message) {
    super(message)
  }

  BadRequestException(String message, Throwable cause) {
    super(message, cause)
  }
}

