package com.codecamp.common.dto

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class Result<T> {
  String message
  T data

  Result success(T data) {
    this.data = data
    this.message = 'Success'
    this
  }

  Result success() {
    return success(null)
  }

  Result error(String message) {
    this.data = null
    this.message = message
    this
  }
}
