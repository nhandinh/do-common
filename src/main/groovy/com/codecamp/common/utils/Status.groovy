package com.codecamp.common.utils

enum Status {
  ACTIVE,
  DEACTIVE
}
