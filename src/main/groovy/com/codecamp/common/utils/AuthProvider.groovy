package com.codecamp.common.utils

import com.fasterxml.jackson.annotation.JsonCreator

enum AuthProvider {
  LOCAL('local'),
  FACEBOOK('facebook'),
  GOOGLE('google'),
  GITHUB('github')

  final String value

  AuthProvider(String value) {
    this.value = value
  }

  @JsonCreator
  static AuthProvider of(String value) {
    switch (value) {
      case LOCAL.value:
        return LOCAL
      case FACEBOOK.value:
        return FACEBOOK
      case GOOGLE.value:
        return GOOGLE
      case GITHUB.value:
        return GITHUB
      default:
        throw new IllegalArgumentException("Invalid AuthProvider ${value}")
    }
  }
}