package com.codecamp.common.utils

/**
 * Create by dhnhan on 28/12/2019
 * Roles for application
 */

enum Role {
  MOD,
  ADMIN,
  USER
}