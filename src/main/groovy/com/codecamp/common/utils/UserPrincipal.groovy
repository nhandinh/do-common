package com.codecamp.common.utils

import com.codecamp.common.domains.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

/**
 * Create by dhnhan on 28/12/2019
 */

class UserPrincipal implements UserDetails {
  Long id
  String email
  String password
  Collection<? extends GrantedAuthority> authorities

  UserPrincipal(Long id, String email, String password) {
    this.id = id
    this.email = email
    this.password = password
    this.authorities = []
  }

  static UserPrincipal create(User user) {
    return new UserPrincipal(
        user.getId(),
        user.getEmail(),
        user.getPassword()
    )
  }

  @Override
  String getPassword() {
    password
  }

  @Override
  String getUsername() {
    email
  }

  @Override
  boolean isAccountNonExpired() {
    true
  }

  @Override
  boolean isAccountNonLocked() {
    true
  }

  @Override
  boolean isCredentialsNonExpired() {
    true
  }

  @Override
  boolean isEnabled() {
    return true
  }
}
