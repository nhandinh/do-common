package com.codecamp.common.utils

import java.text.Normalizer
import java.util.regex.Pattern

final class AppUtil {
  static final Pattern NONLATIN = Pattern.compile('[^\\w-]')
  static final Pattern WHITESPACE = Pattern.compile('[\\s]')

  static String stringToSlug(String input) {
    String noWhitespace = WHITESPACE.matcher(input).replaceAll('-')
    String normalized = Normalizer.normalize(noWhitespace, Normalizer.Form.NFD)
    String slug = NONLATIN.matcher(normalized).replaceAll('')
    return slug.toLowerCase(Locale.ENGLISH)
  }
}
