package com.codecamp.common.auth

import groovy.util.logging.Slf4j
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Create by dhnhan on 28/12/2019
 */

@Slf4j
class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

  @Override
  void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
      throws IOException, ServletException {
    log.error("Responding with unauthorized error. Message - ${e.message}")
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getLocalizedMessage())
  }
}
