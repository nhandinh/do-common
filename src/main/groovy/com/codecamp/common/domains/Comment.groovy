package com.codecamp.common.domains

import javax.persistence.*

@Entity
@Table(name = 'comment')
class Comment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(nullable = false)
  String content

  Date createDate

  Long parentId

  @ManyToOne
  @JoinColumn(name = 'post_id')
  Post post

  @ManyToOne
  @JoinColumn(name = 'user_id')
  User user
}
