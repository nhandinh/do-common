package com.codecamp.common.domains

import com.codecamp.common.utils.Status
import com.fasterxml.jackson.annotation.JsonIgnore

import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = 'community')
class Community {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(nullable = false, unique = true)
  @NotEmpty(message = 'Please provider name')
  String name

  String description

  String banner

  String upVote

  String downVote

  String avatar

  @Column(unique = true, nullable = false)
  String slug

  @Enumerated(EnumType.STRING)
  Status status

  @JsonIgnore
  @OneToMany(mappedBy = 'community')
  Set<Tag> tags

  @JsonIgnore
  @OneToMany(mappedBy = 'community')
  Set<Post> posts

  @JsonIgnore
  @OneToMany(mappedBy = 'community')
  Set<UserCommunity> userCommunities
}
