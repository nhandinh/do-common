package com.codecamp.common.domains

import com.fasterxml.jackson.annotation.JsonIgnore

import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = 'tag')
class Tag {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @NotEmpty(message = 'Please provide name')
  @Column(nullable = false, unique = true)
  String name

  String description

  String titleColor

  String background

  String color

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = 'community_id', nullable = false)
  Community community

  @JsonIgnore
  @OneToMany(mappedBy = 'tag')
  Set<Post> posts
}
