package com.codecamp.common.domains

import javax.persistence.*

@Entity
@Table(name = 'media')
class Media {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  String slug

  String fileName

  @ManyToOne
  @JoinColumn(name = 'user_id')
  User user
}
