package com.codecamp.common.domains

import javax.persistence.*

@Entity
@Table(name = 'vote')
class Vote {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(nullable = false)
  Boolean type

  Date createDate

  @ManyToOne
  @JoinColumn(name = 'post_id')
  Post post

  @ManyToOne
  @JoinColumn(name = 'user_id')
  User user
}
