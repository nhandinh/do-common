package com.codecamp.common.domains

import com.fasterxml.jackson.annotation.JsonIgnore

import javax.persistence.*

@Entity
@Table(name = 'post')
class Post {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(nullable = false)
  String title

  Date createDate

  String slug

  String content

  @ManyToOne
  @JoinColumn(name = 'tag_id')
  Tag tag

  @JsonIgnore
  @OneToMany(mappedBy = 'post')
  Set<Comment> comments

  @ManyToOne
  @JoinColumn(name = 'community_id')
  Community community

  @JsonIgnore
  @OneToMany(mappedBy = 'post')
  Set<Vote> votes

}
