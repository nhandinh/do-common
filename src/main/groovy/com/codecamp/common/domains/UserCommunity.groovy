package com.codecamp.common.domains

import com.codecamp.common.utils.Role

import javax.persistence.*

@Entity
@Table(name = 'user_community')
class UserCommunity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @ManyToOne
  @JoinColumn(name = 'user_id')
  User user

  @ManyToOne()
  @JoinColumn(name = 'community_id')
  Community community

  @Enumerated(EnumType.STRING)
  Role role
}
