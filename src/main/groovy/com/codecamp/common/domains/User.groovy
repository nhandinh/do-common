package com.codecamp.common.domains

import com.codecamp.common.utils.AuthProvider
import com.codecamp.common.utils.Role
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude

import javax.persistence.*

@Entity
@Table(name = 'user')
@JsonInclude(JsonInclude.Include.NON_NULL)
class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(nullable = false)
  String name

  @Column(nullable = false, unique = true)
  String email

  String imageUrl

  @JsonIgnore
  String password

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  AuthProvider provider

  String providerId

  @Temporal(TemporalType.TIMESTAMP)
  Date birthDate

  @Column(nullable = false)
  Boolean active

  @JsonIgnore
  @OneToMany(mappedBy = 'user')
  Set<Comment> comments

  @JsonIgnore
  @OneToMany(mappedBy = 'user')
  Set<Vote> votes

  @JsonIgnore
  @OneToMany(mappedBy = 'user')
  Set<Media> medias

  @JsonIgnore
  @OneToMany(mappedBy = 'user')
  Set<UserCommunity> userCommunities

}
