package com.codecamp.common.provider

import com.codecamp.common.config.AppProperties
import com.codecamp.common.utils.UserPrincipal
import groovy.util.logging.Slf4j
import io.jsonwebtoken.*
import org.springframework.security.core.Authentication

/**
 * Create by dhnhan on 28/12/2019
 */

@Slf4j
class TokenProvider {

  AppProperties appProperties

  TokenProvider(AppProperties appProperties) {
    this.appProperties = appProperties
  }

  String createToken(Authentication authentication) {
    UserPrincipal userPrincipal = (UserPrincipal) authentication.principal
    Date expireDate = new Date(new Date().time + this.appProperties.auth.tokenExpirationMsec)
    Jwts.builder()
        .setSubject(Long.toString(userPrincipal.id))
        .setIssuedAt(new Date())
        .setExpiration(expireDate)
        .signWith(SignatureAlgorithm.HS512, this.appProperties.auth.tokenSecret)
        .compact()
  }

  String createToken(String userId, long expiresIn) {
    long expire = expiresIn == 0 ? this.appProperties.auth.tokenExpirationMsec : expiresIn
    Date expireDate = new Date(new Date().time + expire)
    Jwts.builder()
        .setSubject(userId)
        .setIssuedAt(new Date())
        .setExpiration(expireDate)
        .signWith(SignatureAlgorithm.HS512, this.appProperties.auth.tokenSecret)
        .compact()
  }

  Long getUserIdFromToken(String token) {
    Claims claims = Jwts.parser()
        .setSigningKey(this.appProperties.auth.tokenSecret)
        .parseClaimsJws(token)
        .body
    Long.parseLong(claims.getSubject())
  }

  boolean validateToken(String authToken) {
    try {
      Jwts.parser().setSigningKey(this.appProperties.auth.tokenSecret).parseClaimsJws(authToken)
      return true
    } catch (SignatureException ex) {
      log.error('Invalid JWT signature')
    } catch (MalformedJwtException ex) {
      log.error('Invalid JWT token')
    } catch (ExpiredJwtException ex) {
      log.error('Expired JWT token')
    } catch (UnsupportedJwtException ex) {
      log.error('Unsupported JWT token')
    } catch (IllegalArgumentException ex) {
      log.error('JWT claims string is empty')
    }
    false
  }

}
