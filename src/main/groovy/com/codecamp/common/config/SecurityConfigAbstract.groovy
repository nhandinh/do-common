package com.codecamp.common.config

import com.codecamp.common.auth.RestAuthenticationEntryPoint
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.web.filter.OncePerRequestFilter

/**
 * Create by dhnhan on 28/12/2019
 */

abstract class SecurityConfigAbstract extends WebSecurityConfigurerAdapter {

  abstract OncePerRequestFilter getTokenAuthenticationFilter()

  @Override
  AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean()
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .cors()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .csrf()
        .disable()
        .formLogin()
        .disable()
        .httpBasic()
        .disable()
        .exceptionHandling()
        .authenticationEntryPoint(new RestAuthenticationEntryPoint())
  }
}
