package com.codecamp.common.config

import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Create by dhnhan on 28/12/2019
 */

class MvcConfig implements WebMvcConfigurer {

  static final long MAX_AGE_SECS = 3600

  @Override
  void addCorsMappings(CorsRegistry registry) {
    registry.addMapping('/**')
        .allowedOrigins('*')
        .allowedMethods('GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS')
        .allowedHeaders("*")
        .allowCredentials(true)
        .maxAge(MAX_AGE_SECS)
  }
}

