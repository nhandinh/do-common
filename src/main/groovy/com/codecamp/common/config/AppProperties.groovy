package com.codecamp.common.config

/**
 * Create by dhnhan on 28/12/2019
 */

class AppProperties {
  final Auth auth = new Auth()
  final OAuth2 oauth2 = new OAuth2()

  static class Auth {
    String tokenSecret
    long tokenExpirationMsec
  }

  static class OAuth2 {

    List<String> authorizedRedirectUris = []

    List<String> getAuthorizedRedirectUris() {
      return authorizedRedirectUris
    }

    OAuth2 authorizedRedirectUris(List<String> authorizedRedirectUris) {
      this.authorizedRedirectUris = authorizedRedirectUris
      return this
    }
  }
}
